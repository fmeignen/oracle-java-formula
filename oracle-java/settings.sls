{% set p  = salt['pillar.get']('java', {}) %}
{% set g  = salt['grains.get']('java', {}) %}

{%- set java_home            = salt['grains.get']('java_home', salt['pillar.get']('java_home', '/opt/jre')) %}

{%- set default_version_name = 'jre1.6.0_45' %}
{%- set default_prefix       = '/opt' %}
{%- set default_source_url   = 'http://ftp.adullact.org/deploiement/j/jre-6u45-linux-x64.tgz' %}
{%- set tarball_file   = 'jre-6u45-linux-x64.tgz' %}
{%- set default_source_hash  = '5a912f027461157dc488f21228a9efc34a72c059645334e106368347e410aa83' %}

{%- set version_name         = g.get('version_name', p.get('version_name', default_version_name)) %}
{%- set source_url           = g.get('source_url', p.get('source_url', default_source_url)) %}


{%- if source_url == default_source_url %}
  {%- set source_hash        = default_source_hash %}
{%- else %}
  {%- set source_hash        = g.get('source_hash', p.get('source_hash', '')) %}
{%- endif %}

{%- set prefix               = g.get('prefix', p.get('prefix', default_prefix)) %}
{%- set java_real_home       = prefix + '/' + version_name %}
{%- set jre_lib_sec          = java_real_home + '/jre/lib/security' %}

{%- set java = {} %}
{%- do java.update( { 'version_name'   : version_name,
                      'source_url'     : source_url,
                      'source_hash'    : source_hash,
                      'java_home'      : java_home,
                      'prefix'         : prefix,
                      'java_real_home' : java_real_home,
                      'jre_lib_sec'    : jre_lib_sec,
                    } ) %}
